# Copyright 2024 Lisa Carrier <lisa.carrier@clever-cloud.com>
# Distributed under the terms of the GNU General Public License v2

require github [ user=thesofproject release=v${PV} suffix=tar.gz ]

SUMMARY="Sound Open Firmware (SOF) binary files"
DESCRIPTION="Sound Open Firmware is an open source audio DSP firmware and SDK
that provides audio firmware infrastructure and development tools for developers
who are interested in audio or signal processing on modern DSPs."

LICENCES="BSD-2 BSD-3 ISC"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    tools [[ description = [
        Debug and tuning tools: mtrace-reader, sof-ctl, sof-probes
    ] ]]
"

DEPENDENCIES="
    run:
        tools? (
            sys-libs/glibc
            sys-sound/alsa-lib
        )
"

src_install() {
    insinto /usr/$(exhost --target)/lib/firmware/intel
    doins -r sof*
    if option tools; then
        dobin tools/sof-*
    fi
}

