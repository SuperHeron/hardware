# Copyright 2016-2023 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=Ryochan7 tag=v${PV} ] \
    setup-py [ import=setuptools has_bin=true multibuild=false blacklist=2 test=pytest ] \
    udev-rules [ udev_files=[ scripts/69-sc-controller.rules ] ] \
    freedesktop-desktop \
    freedesktop-mime \
    gtk-icon-cache

SUMMARY="User-mode driver, mapper and GTK3 based GUI for Steam Controller, DS4 and similar"

LICENCES="
    CC0 [[ note = [ images under the images/ directory ] ]]
    BSD-3 [[ note = [ lib/enum.py ] ]]
    GPL-2
    LGPL-2.1 [[ note = [ lib/usb1.py and lib/libusb1.py ] ]]
    PSF-2.2 [[ note = [ lib/jsonencoder.py ] ]]
    ZLIB [[ note = [ scripts/gamecontrollerdb.txt ] ]]
"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    ( providers: eudev systemd ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build+run:
        dev-python/evdev[python_abis:*(-)?]
        dev-python/pycairo[python_abis:*(-)?]
        dev-python/pylibacl[python_abis:*(-)?]
        dev-python/vdf[python_abis:*(-)?]
        gnome-bindings/pygobject:3[python_abis:*(-)?]
    run:
        dev-libs/libusb:1
        gnome-desktop/adwaita-icon-theme
        gnome-desktop/librsvg:2[gobject-introspection]
        x11-libs/gtk+:3[>=3.22]
        providers:eudev? ( sys-apps/eudev )
        providers:systemd? ( sys-apps/systemd )
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/${PN}-0.4.8.9-fix-tests-pr73.patch
)

PYTEST_PARAMS=(
    # skip failing tests, last checked: 0.4.8.9
    "-k not test_packages and not test_146_1 and not test_146_2"
)

src_install() {
    setup-py_src_install

    install_udev_files

    edo rm -rf "${IMAGE}"/usr/lib

    # remove empty directories
    edo find "${IMAGE}" -type d -empty -delete
}

pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    freedesktop-mime_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    freedesktop-mime_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

