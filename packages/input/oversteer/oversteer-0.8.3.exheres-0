# Copyright 2021-2024 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=berarma tag=v${PV} ] \
    meson \
    python [ blacklist=2 multibuild=false ] \
    udev-rules \
    gtk-icon-cache

SUMMARY="Application to configure steering wheels on Linux"

LICENCES="GPL-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

# tests currently only validate appstream/desktop files
RESTRICT="test"

DEPENDENCIES="
    build:
        sys-devel/gettext
    build+run:
        dev-python/evdev[python_abis:*(-)?]
        dev-python/matplotlib[gtk3][python_abis:*(-)?]
        dev-python/pyudev[python_abis:*(-)?]
        dev-python/pyxdg[python_abis:*(-)?]
        dev-python/scipy[python_abis:*(-)?]
        gnome-bindings/pygobject:3[python_abis:*(-)?]
"

MESON_SRC_CONFIGURE_PARAMS=(
    -Dpython=${PYTHON}
    -Dudev_rules_dir=${UDEVRULESDIR}
)

src_install() {
    meson_src_install

    python_bytecompile
}

