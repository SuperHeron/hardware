# Copyright 2010-2012 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

SCM_REPOSITORY="git://git.sukimashita.com/sbmanager.git"

require scm-git autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.11 ] ]

SUMMARY="Manage iPhone/iPod Touch SpringBoard icons from the computer"
DESCRIPTION="
SBManager is a program which allows to manage the SpringBoard icons on any iPhone/iPod
Touch device running firmware 3.1 or later:
* Show SpringBoard icons as they appear on the device display.
* Manage icons using drag and drop and upload changes back to the device.
* Smooth hardware accelerated animations using the Clutter library.
* Show the device's battery level and current clock.
* Supports 'five icon dock' extension.
"
HOMEPAGE="http://www.libimobiledevice.org"
DOWNLOADS=""

REMOTE_IDS=""

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="nautilus [[ description = [ Install a Nautilus extension. ] ]]"

DEPENDENCIES="
    build:
        dev-util/pkg-config
    build+run:
        app-pda/libimobiledevice[>=1.0.0]
        app-pda/usbmuxd[>=1.0.3]
        dev-libs/glib:2[>=2.14.1]
        dev-libs/libplist[>=1.0]
        x11-libs/clutter:1[>=1.0.6]
        x11-libs/clutter-gtk:1.0[>=1.0.0]
        x11-libs/gtk+:2[>=2.16]
        nautilus? (
            dev-libs/at-spi2-core[>=2.52.0]
            dev-libs/expat[>=2.0.1]
            dev-libs/gnutls[>=2.8.6]
            dev-libs/libgcrypt[>=1.4.5]
            dev-libs/libgpg-error[>=1.7]
            dev-libs/libtasn1[>=2.5]
            dev-libs/libxml2:2.0[>=2.7.7]
            gnome-desktop/nautilus[>=2.21.2]
            media-libs/fontconfig[>=2.8.0]
            media-libs/freetype:2[>=2.3.11]
            media-libs/libpng[>=1.2.43]
            virtual/usb:1
            x11-dri/mesa[>=7.7.1]
            x11-libs/cairo[>=1.8.10]
            x11-libs/libX11[>=1.3.2]
            x11-libs/libXau[>=1.0.5]
            x11-libs/libXcomposite[>=0.4.1]
            x11-libs/libXdamage[>=1.1.2]
            x11-libs/libXdmcp[>=1.0.3]
            x11-libs/libXext[>=1.1.1]
            x11-libs/libXfixes[>=4.0.4]
            x11-libs/libXi[>=1.3]
            x11-libs/libXrender[>=0.9.5]
            x11-libs/pango[>=1.26.2]
            x11-libs/pixman:1[>=0.16.6]
        )
"

src_prepare() {
    default

    if ! option nautilus ; then
        edo sed -i -e "/nautilus/Id" configure.ac
        edo sed -i -e "s:\(SUBDIRS = data src po\) nautilus:\1:" Makefile.am
    fi
    edo sed -i -e "s:\-Werror:${CFLAGS}:" configure.ac

    AT_M4DIR=( m4 )

    eautoreconf
}

src_configure() {
    export libbz2_LIBS="-lbzip2"
    export libbz2_CFLAGS="${CFLAGS}"

    default
}

