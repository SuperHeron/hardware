# Copyright 2016 Wouter van Kesteren <woutershep@gmail.com>
# Distributed under the terms of the GNU General Public License v2

CLHPP_REV=0bdbbfe5ecda42cff50c96cc5e33527f42fcbd45

require github [ user=KhronosGroup project=OpenCL-Headers tag=v${PV} ] cmake

SUMMARY="Khronos's OpenCL (Open Computing Language) header files"
HOMEPAGE="https://www.khronos.org/registry/OpenCL"
DOWNLOADS+="
    https://github.com/KhronosGroup/OpenCL-CLHPP/raw/${CLHPP_REV}/include/CL/cl2.hpp -> ${PNV}-cl2.hpp
    https://github.com/KhronosGroup/OpenCL-CLHPP/raw/${CLHPP_REV}/include/CL/opencl.hpp -> ${PNV}-opencl.hpp
"

LICENCES="Apache-2.0"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS=""

DEPENDENCIES=""

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DCMAKE_DISABLE_FIND_PACKAGE_Python3:BOOL=TRUE
    -DCMAKE_INSTALL_DATADIR:PATH=/usr/$(exhost --target)/lib
)

CMAKE_SRC_CONFIGURE_TESTS=(
    '-DBUILD_TESTING:BOOL=TRUE -DBUILD_TESTING:BOOL=FALSE'
    '-DOPENCL_HEADERS_BUILD_CXX_TESTS:BOOL=TRUE -DOPENCL_HEADERS_BUILD_CXX_TESTS:BOOL=FALSE'
    '-DOPENCL_HEADERS_BUILD_TESTING:BOOL=TRUE -DOPENCL_HEADERS_BUILD_TESTING:BOOL=FALSE'
)

src_install() {
    cmake_src_install

    # OpenCL C++ Bindings
    insinto /usr/$(exhost --target)/include/CL
    newins "${FETCHEDDIR}"/${PNV}-cl2.hpp cl2.hpp
    newins "${FETCHEDDIR}"/${PNV}-opencl.hpp opencl.hpp

    # We're not interested in Direct3D things
    edo rm "${IMAGE}"/usr/$(exhost --target)/include/CL/cl_{dx9_media_sharing{,_intel},d3d10,d3d11}.h
}

