# Copyright 2022 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

export_exlib_phases src_compile src_test src_install

SUMMARY="C library and tools for interacting with the linux GPIO character device"
DESCRIPTION="
Since linux 4.8 the GPIO sysfs interface is deprecated. User space should use
the character device instead. This library encapsulates the ioctl calls and
data structures behind a straightforward API.
Unfortunately interacting with the linux device file can no longer be done
using only standard command-line tools. This is the reason for creating a
library encapsulating the cumbersome, ioctl-based kernel-userspace interaction
in a set of convenient functions and opaque data structures.
Additionally this project contains a set of command-line tools that should
allow an easy conversion of user scripts to using the character device."

HOMEPAGE="https://git.kernel.org/pub/scm/libs/${PN}/${PN}.git/about/"
DOWNLOADS="mirror://kernel/software/libs/${PN}/${PNV}.tar.xz"

LICENCES="
    GPL-2    [[ note = [ gpio-tools and test suites ] ]]
    LGPL-2.1 [[ note = [ libgpiod library ] ]]
    LGPL-3   [[ note = [ C++ bindings ] ]]
"
SLOT="0"
MYOPTIONS="doc"

DEPENDENCIES="
    build:
        sys-apps/help2man
        sys-kernel/linux-headers[>=4.8.0]
        doc? ( app-doc/doxygen )
    test:
        dev-cpp/catch [[ note = [ only for cxx bindings' tests ] ]]
        dev-libs/glib:2[>=2.50]
        dev-util/bats-core
        sys-apps/kmod[>=18]
        sys-apps/systemd[>=215] [[ note = [ udev ] ]]
"

UPSTREAM_RELEASE_NOTES="https://git.kernel.org/pub/scm/libs/libgpiod/libgpiod.git/tree/NEWS"

# Needs the gpio-mockup kernel module and root
RESTRICT="test"

DEFAULT_SRC_CONFIGURE_PARAMS+=(
    --enable-tools
    --enable-bindings-cxx
    --disable-python-bindings
    --disable-static
)
DEFAULT_SRC_CONFIGURE_TESTS+=(
    "--enable-tests --disable-tests"
)

libgpiod_src_compile() {
    default

    option doc && emake doc
}

libgpiod_src_test() {
    default

    edo pushd tests
    edo ./gpiod-test
    edo popd
    edo pushd tools
    edo ./gpio-tools-test
    edo popd
    edo pushd bindings/cxx/tests
    edo ./gpiod-cxx-test
    edo popd
}

libgpiod_src_install() {
    default

    if option doc; then
        docinto html
        dodoc doc/html/*
    fi

}

