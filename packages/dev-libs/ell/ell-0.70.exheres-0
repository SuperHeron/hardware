# Copyright 2018 Tom Briden <tom@decompile.me.uk>
# Distributed under the terms of the GNU General Public License v2

SUMMARY="Embedded Linux Library"
DESCRIPTION="
The Embedded Linux* Library (ELL) provides core, low-level functionality for
system daemons. It typically has no dependencies other than the Linux kernel,
C standard library, and libdl (for dynamic linking). While ELL is designed to be
efficient and compact enough for use on embedded Linux platforms, it is not
limited to resource-constrained systems.
"
HOMEPAGE="https://01.org/ell"
DOWNLOADS="mirror://kernel/linux/libs/${PN}/${PNV}.tar.xz"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~armv8"
MYOPTIONS=""

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        dev-libs/glib:2[>=2.32]
    test:
        dev-libs/openssl:*   [[ note = [ openssl command line tool ] ]]
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-glib
    --disable-examples
)
DEFAULT_SRC_CONFIGURE_TESTS=(
    '--enable-tests --disable-tests'
)

#test results have been inconsistent with different tests failing on different 
#machines
RESTRICT="test"

